<?php
namespace Obvi\Config;

class Config {
	const MODE = '';
	const DEBUG = false;

	/* Database connection. */
	const DB_HOST = '127.0.0.1';
	const DB_NAME =  '';
	const DB_USERNAME = '';
	const DB_PASSWORD = '';
}
