--
-- Table structure for table `AliasLookups`
--

DROP TABLE IF EXISTS `AliasLookups`;
CREATE TABLE `AliasLookups` (
  `url_alias` varchar(24) NOT NULL,
  `referer` varchar(4096) NOT NULL,
  `remote_host` varchar(40) NOT NULL,
  `remote_addr` varchar(256) DEFAULT NULL,
  `time_accessed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `url_alias_lookup` (`url_alias`),
  KEY `time_accessed_lookup` (`time_accessed`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


--
-- Table structure for table `UrlAliases`
--

DROP TABLE IF EXISTS `UrlAliases`;
CREATE TABLE `UrlAliases` (
  `alias` varchar(24) NOT NULL,
  `url` varchar(2048) NOT NULL,
  `creating_user_id` int(10) unsigned NOT NULL,
  `time_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`alias`),
  UNIQUE KEY `alias_UNIQUE` (`alias`),
  KEY `user_id_lookup` (`creating_user_id`),
  CONSTRAINT `creating_user_id_fk` FOREIGN KEY (`creating_user_id`) REFERENCES `Users` (`user_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
CREATE TABLE `Users` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(16) NOT NULL,
  `user_salt` varchar(64) NOT NULL,
  `password_hash` varchar(512) NOT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `Userscol_UNIQUE` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

