<?php

require_once(dirname(__FILE__) . '/../vendor/autoload.php');

function authenticate () {
	return true;
}

function authenticate_admin() {
	return authenticate();
}


$config = array();
$config['mode'] = \Obvi\Config\Config::MODE;
$config['debug'] = \Obvi\Config\Config::DEBUG;

$app = new \Slim\Slim($config);

$app->get('/', function () {
	echo "Home";
});


$app->get('/:shortcut', function ($shortcut) {
	// Query database to get full URL.


	// Log request.

	// Forward to destination.
	echo $shortcut;
});


$app->get('/manage-urls', function() {
	echo "Manage your URLs.";
});


$app->get('/admin', 'authenticate_admin', function() {
	echo "Admin";
});



$app->run();

